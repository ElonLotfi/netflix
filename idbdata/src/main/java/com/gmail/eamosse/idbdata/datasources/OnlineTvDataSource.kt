package com.gmail.eamosse.idbdata.datasources

import com.gmail.eamosse.idbdata.api.response.CategoryResponse
import com.gmail.eamosse.idbdata.api.response.FilmResponse
import com.gmail.eamosse.idbdata.api.response.TokenResponse
import com.gmail.eamosse.idbdata.api.response.TvResponse
import com.gmail.eamosse.idbdata.api.service.TvService
import com.gmail.eamosse.idbdata.utils.Result
import com.gmail.eamosse.idbdata.utils.parse
import com.gmail.eamosse.idbdata.utils.safeCall

internal class OnlineTvDataSource(private val service: TvService) {



    /**
     * Récupérer le token du serveur
     * @return [Result<Token>]
     * Si [Result.Succes], tout s'est bien passé
     * Sinon, une erreur est survenue
     */
    suspend fun getToken(): Result<TokenResponse> {
        return safeCall {
            val response = service.getToken()
            response.parse()
        }
    }


    suspend fun getTvCategories(): Result<List<CategoryResponse.Genre>> {
        return safeCall {
            val response = service.getTvCategory()
            when (val result = response.parse()) {
                is Result.Succes -> Result.Succes(result.data.genres)
                is Result.Error -> result
            }
        }
    }

    /**
     * Get TV via Gender
     */
    suspend fun getTvByCategory(categoryId :String): Result<List<TvResponse.Tv>> {
        return safeCall {
            val response = service.getTvByGategory(categoryId)
            when (val result = response.parse()) {
                is Result.Succes -> Result.Succes(result.data.tv_list)
                is Result.Error -> result
            }
        }
    }

    /**
     * Get TV via id
     */

    suspend fun getTvById(tvId: String): Result<TvResponse.Tv> {
        return safeCall {
            val response = service.getTvById(tvId.toInt())
            when (val result = response.parse()) {
                is Result.Succes -> Result.Succes(result.data)
                is Result.Error -> result
            }
        }
    }


}