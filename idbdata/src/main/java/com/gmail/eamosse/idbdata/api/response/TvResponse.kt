package com.gmail.eamosse.idbdata.api.response

import com.gmail.eamosse.idbdata.data.Film
import com.gmail.eamosse.idbdata.data.Tv
import com.google.gson.annotations.SerializedName

data class TvResponse(
    @SerializedName("results")
    val tv_list: List<Tv>
)
{
    data class Tv(

        @SerializedName("id")
        val id: Int,

        @SerializedName("original_name")
        val name: String,
        @SerializedName("overview")
        val overview: String,
        @SerializedName("poster_path")
        val poster_path: String,
        @SerializedName("vote_average")
        val vote: Float,
        @SerializedName("first_air_date")
        val date: String
    )
}

internal fun TvResponse.Tv.toTv() = Tv(
     id = id,
     name = name,
     overview = overview,
     poster = "https://image.tmdb.org/t/p/original" + poster_path ,
     vote = vote,
     date = date

)