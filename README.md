# The Moovie App <img src="https://media.giphy.com/media/Wsju5zAb5kcOfxJV9i/giphy.gif" width="170x"> <br /> 

<img src="http://www.ituniversity-mg.com/images/logo_mbds.jpg" width="400px" align="right">



<br />

## Participant :
| *Nom et Prenom* | *Pseudo* |
| ------ | ------ |
| M'hamed Lotfi | ElonLotfi |
| Saidan Rachid | Rachid |





---


Bonjour, <br /> 

Collaborateur du projet :<br>

🙍🏽‍♂️ [@Lotfi](https://github.com/ElonLotfi) <br>
🙍🏽‍♂️ [@Rachid](https://github.com/Rachid?) <br>



encadrant 👨🏽‍💼[@M.Amosse] <br /> 

<br /> 
<br /> 

## Objectif de l'application :

L'objectif de cette application est de permettre à l'utilisateur de parcourir les catégories de films et de séries, de trouver la liste des films et des séries par catégories et d'afficher ses informations telles que le titre, la description et le score. <br>

## Fonctionnalités implémentées  <img src="https://media.giphy.com/media/26vwfMVM6nlEkwftUj/giphy.gif" width="40px"> <br /> 
✅ Afficher les catégories de films <br>
✅ Parcourir les films par catégorie <br>
✅ Afficher les détails de chaque film<br>
✅ Afficher les catégories de series <br>
✅ Parcourir les series par catégorie<br>
✅ Afficher les détails de chaque serie <br>
✅ Internationalisation de l'application (anglais + francais) <br>
✅ Gestion de la navigation avec Navigation-fragment <br>
✅ Mise en place de l'architecture [MVVM](https://medium.com/androidmood/comprendre-larchitecture-mvvm-sur-android-aa285e4fe9dd)(Model View View Model)<br>
✅ Gestion de données via une API et Room <br>


## Api  <br /> 

⚔️ /genre/movie/lis : Permet de retrouver la liste des films par catégorie <br>
⚔️ discover/movie : Permet de récupérer les catégories de films <br>
⚔️ movie/{id} : Permet de récupérer les données d'un film via son id <br>
⚔️ genre/tv/list : Permet de retrouver la liste des series par catégorie<br>
⚔️ discover/tv : Permet de récupérer les catégories de séries <br>
⚔️ tv/{id} : Permet de récupérer les données d'une serie via son id <br>

## librairies utilisées  <br /> 

⚔️ Navigation-fragment. <br>
⚔️ Hilt : Injection de dépendances . <br>
⚔️ Gson/Moshi : Sérialisation et Désérialisation JSON. <br>
⚔️ Retrofit: Pour consommer l'API The Moovie DB. <br>
⚔️ Glide : Pour afficher les images . <br>
⚔️ OkHttp: Client HTTP <br>


## Video <br />

[![Watch the video](https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/icon.png)](https://www.youtube.com/watch?v=--7xrZ0iKQ0)



## Captures d'écrans des principales vues <br />

Home :

 <p float="left">
   <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/home.png" height="420px" width="240px">
</p>

---

Film :
<p float="left">
  <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/filmcategory.png" height="420px" width="240px">
 <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/filmsbycategory.png" height="420px" width="240px"> 
  <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/filmdetails.png" height="420px" width="240px"> 
</p>

---

Series :
<p float="left">
  <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/seriescategory.png" height="420px" width="240px">
 <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/seriesbycategory.png" height="420px" width="240px"> 
  <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/seriedetails.png" height="420px" width="240px"> 
</p>

---

Information :
<p float="left">
 <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/information1.png" height="420px" width="240px">
 <img src="https://gitlab.com/ElonLotfi/netflix/-/raw/master/demo/information2.png" height="420px" width="240px"> 
</p>

## Une application Web develppé à l'aide de:<br>
 `Android & Kotlin & Architecture Components & Room`<br>







