package com.gmail.eamosse.imdb.ui.film.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.gmail.eamosse.imdb.R
import com.gmail.eamosse.imdb.databinding.FragmentFilmListBinding
import com.gmail.eamosse.imdb.ui.film.FilmViewModel
import com.gmail.eamosse.imdb.ui.film.adapter.FilmsAdapter


class FilmFragment : Fragment() {

    private val args: com.gmail.eamosse.imdb.ui.film.fragment.FilmFragmentArgs by navArgs()
    private val homeViewModel: FilmViewModel by viewModel()
    private lateinit var binding : FragmentFilmListBinding



    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFilmListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(
            R.string.film_genre
        ) + " : " + args.categoryName

        with(homeViewModel) {
            token.observe(viewLifecycleOwner, Observer {
                //récupérer les films
                getFilmsByCategory(args.categoryId)
            })

            films.observe(viewLifecycleOwner, Observer {

                binding.filmList.adapter = FilmsAdapter(it)
            })

            error.observe(viewLifecycleOwner, Observer {
                //afficher l'erreur
            })
        }
    }
}
