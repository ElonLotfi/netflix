package com.gmail.eamosse.imdb.ui.tv.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.gmail.eamosse.imdb.databinding.FragmentTvCategoryBinding
import com.gmail.eamosse.imdb.ui.home.HomeFragmentDirections
import com.gmail.eamosse.imdb.ui.tv.TvViewModel
import com.gmail.eamosse.imdb.ui.tv.adapter.TvCategoryAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class TvCategoryFragment : Fragment() {
    private lateinit var binding: FragmentTvCategoryBinding
    private val tvViewModel: TvViewModel by viewModel()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTvCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(tvViewModel) {
            token.observe(viewLifecycleOwner, Observer {
                //récupérer les catégories
                getTvCategories()
            })

            categories.observe(viewLifecycleOwner, Observer {
                binding.categoryList.adapter = TvCategoryAdapter(it)
            })

            error.observe(viewLifecycleOwner, Observer {
                //afficher l'erreur
            })
        }

        binding.buttonHome.setOnClickListener {
            val action = HomeFragmentDirections.actionNavigationHomeSelf()
            NavHostFragment.findNavController(this)
                .navigate(action)
        }
    }
}