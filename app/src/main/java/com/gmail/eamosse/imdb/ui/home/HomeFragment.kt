package com.gmail.eamosse.imdb.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.gmail.eamosse.imdb.R

import com.gmail.eamosse.imdb.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding){
            buttonFilms.text = getString(
                R.string.films
            )
            buttonFilms.setOnClickListener {
                val action =
                    HomeFragmentDirections.actionNavigationHomeToNavigationFilmCategory()
                Navigation.findNavController(it).navigate(action)
            }
            buttonTv.text = getString(
                R.string.series
            )
            buttonTv.setOnClickListener {
                val action =
                    HomeFragmentDirections.actionNavigationHomeToNavigationTvCategory()
                Navigation.findNavController(it).navigate(action)
            }

            buttonHome.setOnClickListener {
                val action = HomeFragmentDirections.actionNavigationHomeSelf()
                NavHostFragment.findNavController(this@HomeFragment)
                    .navigate(action)
            }
        }
    }


}