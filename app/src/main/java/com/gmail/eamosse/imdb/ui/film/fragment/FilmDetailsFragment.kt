package com.gmail.eamosse.imdb.ui.film.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gmail.eamosse.imdb.R
import com.gmail.eamosse.imdb.databinding.FragmentFilmDetailsBinding
import com.gmail.eamosse.imdb.ui.film.FilmViewModel


class FilmDetailsFragment : Fragment() {

    private val homeViewModel: FilmViewModel by viewModel()
    private lateinit var binding : FragmentFilmDetailsBinding
    private val args: com.gmail.eamosse.imdb.ui.film.fragment.FilmDetailsFragmentArgs by navArgs()




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFilmDetailsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(homeViewModel) {
            token.observe(viewLifecycleOwner, Observer {
                //récupérer les films
                 getFilmById(args.filmId.toInt())
            })

            film.observe(viewLifecycleOwner, Observer {

                with(binding){
                    (activity as AppCompatActivity).supportActionBar?.title = getString(
                        R.string.film_details
                    ) + " : " + film.value?.name
                    filmName.text = film.value?.name
                    filmdDescription.text = film.value?.description
                    filmRating.rating = (film.value?.vote_average!! /2)

                    val context = filmImg
                    Glide.with(context)
                        .load(film.value?.poster_path)
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.ic_launcher_background)
                        .error(R.drawable.ic_film_load)
                        .skipMemoryCache(false)
                        .into(context)
                }



            })

            error.observe(viewLifecycleOwner, Observer {
                //afficher l'erreur
                println("error" + error.value)
            })
        }
    }

}