package com.gmail.eamosse.imdb.ui.tv.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gmail.eamosse.imdb.R
import com.gmail.eamosse.imdb.databinding.FragmentTvDetailsBinding
import com.gmail.eamosse.imdb.ui.tv.TvViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TvDetailsFragment : Fragment() {

    private val homeViewModel: TvViewModel by viewModel()
    private lateinit var binding : FragmentTvDetailsBinding
    private val args: com.gmail.eamosse.imdb.ui.tv.fragment.TvDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTvDetailsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(homeViewModel) {
            token.observe(viewLifecycleOwner, Observer {
                //récupérer les films
                getTvById(args.tvId.toInt())
            })

            tv.observe(viewLifecycleOwner, Observer {
                with(binding){
                    (activity as AppCompatActivity).supportActionBar?.title = getString(
                        R.string.tv_details
                    ) + " : " + tv.value?.name
                    tvShowName.text = tv.value?.name
                    tvShowDescription.text = tv.value?.overview
                    tvShowRating.rating = (tv.value?.vote!! /2)

                    val context = tvShowImg
                    Glide.with(context)
                        .load(tv.value?.poster)
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.ic_launcher_background)
                        .error(R.drawable.ic_film_load)
                        .skipMemoryCache(false)
                        .into(context)

                }

            })

            error.observe(viewLifecycleOwner, Observer {
                //afficher l'erreur
                println("error" + error.value)
            })
        }
    }

}