package com.gmail.eamosse.imdb.ui.tv.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.lifecycle.Observer
import com.gmail.eamosse.imdb.R
import com.gmail.eamosse.imdb.databinding.FragmentTvListBinding
import com.gmail.eamosse.imdb.ui.tv.adapter.TvAdapter
import com.gmail.eamosse.imdb.ui.tv.TvViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TvFragment : Fragment() {

    private val args: com.gmail.eamosse.imdb.ui.tv.fragment.TvFragmentArgs by navArgs()
    private val homeViewModel: TvViewModel by viewModel()
    private lateinit var binding : FragmentTvListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTvListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = getString(
            R.string.tv_category
        ) + " : " + args.tvName
        with(homeViewModel) {
            token.observe(viewLifecycleOwner, Observer {
                //récupérer les films
                getTvByCategory(args.categoryId)
            })

            tv_list.observe(viewLifecycleOwner, Observer {
                binding.tvList.adapter = TvAdapter(it)
            })

            error.observe(viewLifecycleOwner, Observer {
                //afficher l'erreur
            })
        }
    }
}