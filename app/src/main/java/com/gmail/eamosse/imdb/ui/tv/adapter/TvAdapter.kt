package com.gmail.eamosse.imdb.ui.tv.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gmail.eamosse.idbdata.data.Tv
import com.gmail.eamosse.imdb.R
import com.gmail.eamosse.imdb.databinding.TvListItemBinding
import com.gmail.eamosse.imdb.ui.tv.fragment.TvFragmentDirections


class TvAdapter(private val items: List<Tv>) :
    RecyclerView.Adapter<TvAdapter.ViewHolder>() {


    inner class ViewHolder(private val binding: TvListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Tv) {
            binding.item = item
            val context = binding.tvShowImg
            Glide.with(context)
                .load(item.poster)
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_film_load)
                .skipMemoryCache(false)
                .into(context)


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(TvListItemBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])


        holder.itemView.setOnClickListener {
            val action =
                TvFragmentDirections.actionNavigationTvFragmentToNavigationTvDetails(
                    items[position].id.toString(),
                )
            Navigation.findNavController(it).navigate(action)

        }


    }

}
