package com.gmail.eamosse.imdb.ui.information

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gmail.eamosse.imdb.R
import com.gmail.eamosse.imdb.databinding.FragmentInformationBinding

class InformationFragment : Fragment() {

    private lateinit var binding : FragmentInformationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentInformationBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(
            R.string.information
        )

        with(binding){
            member1Name.text = getString(
                R.string.rachid
            )
            member2Name.text = getString(
                R.string.lotfi
            )
            encadrantName.text =getString(
                R.string.Amosse
            )
            projectDescription.text = getString(
                R.string.project_description
            )

            val context = member1Img
            Glide.with(context)
                .load("https://avatars.githubusercontent.com/u/44263294?v=4")
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_film_load)
                .skipMemoryCache(false)
                .into(context)


            val context1 = member2Img
                Glide.with(context)
                    .load("https://media-exp1.licdn.com/dms/image/C4E03AQFxOkowpJLhPQ/profile-displayphoto-shrink_800_800/0/1577124315219?e=1644451200&v=beta&t=saTSr0CkJ7EiUBIymFundhApTTnZsC7cXyO1tupXhA8")
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_film_load)
                    .skipMemoryCache(false)
                    .into(context1)


            val context2 = encadrantImg
            Glide.with(context)
                .load("https://media-exp1.licdn.com/dms/image/C5603AQHg13iSRaqznA/profile-displayphoto-shrink_800_800/0/1516485131056?e=1644451200&v=beta&t=1J4a6JFgSKOn8f1Nx5RCyCxOK0TzuapgPjAMzHCt1-8")
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_film_load)
                .skipMemoryCache(false)
                .into(context2)
        }



    }
    }

