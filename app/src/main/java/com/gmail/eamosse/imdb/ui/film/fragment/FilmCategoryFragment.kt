package com.gmail.eamosse.imdb.ui.film.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gmail.eamosse.imdb.databinding.FragmentFilmCategoryBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.gmail.eamosse.imdb.ui.film.FilmViewModel
import com.gmail.eamosse.imdb.ui.film.adapter.FilmCategoryAdapter
import com.gmail.eamosse.imdb.ui.home.HomeFragmentDirections


class FilmCategoryFragment : Fragment() {
    private lateinit var binding: FragmentFilmCategoryBinding
    private val filmviewModel: FilmViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFilmCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(filmviewModel) {
            token.observe(viewLifecycleOwner, Observer {
                //récupérer les catégories
                getFilmsCategory()
            })

            categories.observe(viewLifecycleOwner, Observer {
                binding.categoryList.adapter = FilmCategoryAdapter(it)
            })

            error.observe(viewLifecycleOwner, Observer {
                //afficher l'erreur
            })
        }

        binding.buttonHome.setOnClickListener {
            val action = HomeFragmentDirections.actionNavigationHomeSelf()
            NavHostFragment.findNavController(this)
                .navigate(action)
        }
    }
}
